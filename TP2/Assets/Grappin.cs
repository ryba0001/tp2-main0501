using UnityEngine;

public class Grappin : MonoBehaviour
{
    private FixedJoint joint;

    // Start is called before the first frame update
    void Start(){
        Debug.Log("Grappin script started");
    }

    // Update is called once per frame
    void Update(){
        if(Input.GetKey(KeyCode.Space)){
            Debug.Log("Space key pressed");
            if(joint != null){
                Destroy(joint);
                joint = null;
                Debug.Log("FixedJoint destroyed");
            } else {
                Debug.LogWarning("No FixedJoint to destroy");
            }
        }
    }

    void OnCollisionEnter(Collision collision){
        Debug.Log("Collision detected with " + collision.gameObject.name);
        if(joint == null && collision.articulationBody != null){
            joint = this.gameObject.AddComponent<FixedJoint>();
            joint.connectedArticulationBody = collision.articulationBody;
            Debug.Log("FixedJoint created and connected to " + collision.gameObject.name);
        }
    }
}