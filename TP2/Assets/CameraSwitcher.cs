using UnityEngine;

public class CameraSwitcher : MonoBehaviour
{
    public Camera MainCamera;
    public Camera MoufleCamera;
    public Camera BaseCamera;

    void Start()
    {
        // Assurez-vous que seule la caméra principale est active au début
        MainCamera.enabled = true;
        MoufleCamera.enabled = false;
        BaseCamera.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {   
            SwitchCamera(MainCamera);
        }
        else if (Input.GetKeyDown(KeyCode.I))
        {
            SwitchCamera(MoufleCamera);
        }
        else if (Input.GetKeyDown(KeyCode.O))
        {
            SwitchCamera(BaseCamera);
        }
    }

    void SwitchCamera(Camera activeCamera)
    {
        // Désactiver toutes les caméras
        MainCamera.enabled = false;
        MoufleCamera.enabled = false;
        BaseCamera.enabled = false;

        // Activer la caméra sélectionnée
        activeCamera.enabled = true;
    }
}